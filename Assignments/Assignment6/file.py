import sys
import os
import math
from string import whitespace

#Lib of functions to be called in main script

# Define the class
class Address:
	def __init__(self,First,Last,Day,Hours,Min):
		self.First = First
		self.Last  = Last
		self.Day   = Day
		self.Hours = Hours
		self.Min   = Min

class DocumentText:
	def __init__(self,address):
		self.address = address

		self.header = ("\\documentclass[12pt]{article} \n"
			    +  "\\usepackage{geometry} \n"
			    +  "\\begin{document} \n"
			    +  "\\thispagestyle{empty} \n\n")

		self.name = (Address.First + " " + Address.Last + "\n")

		self.body = ()

		self.footer = "\\end{document}\n"

	def WriteLetter(self):
		outputName = str(Address.First(' ')) + ".tex"
		outputFile = open(outputName, "w")
		outputFile.write(self.header)
		outputFile.write(self.name)
#		outputFile.write(self.body)
		outputFile.write(self.footer)



