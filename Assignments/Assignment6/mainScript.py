#Did not need to use another file
#from functions import *
""" 
this is the start of the program that does what the assignment wants
"""
if len(sys.argv) !=2:
	print "python mainScript.py "
else:
	print "Using file from system", str(sys.argv[1])

dataFile = str(sys.argv[1])
# ===================
# OPEN FILES
# ===================
data = open(dataFile,"r")

# ===================
# READ DATA FROM FILE
# ==================

print "Reading from a file " + dataFile

# Calculate Hours 
hours = 0
minutes = 0
for line in data.readlines():
	lines = line.split("\n")

	for n in range(0,len(line)):
		if line[n] == 'h':
			if line[n+1] == 'r':
				hours += int(line[n-1])
# Calculate Minutes
		if line[n] == 'm':
			if line[n+1] == 'i':
				if line[n+2] == 'n':
					minutes += int(line[n-1])
					minutes += 10*(int(line[n-2]))

	data.close()

#Adds extra minutes to hours and other time conversions
extraHours = minutes / 60
totalMinutes = minutes%60
extraHours += hours

# Prints out firstLine, or the employees name
with open(dataFile) as file:
        firstLine = file.readline()

#prints the data to console
print firstLine, extraHours, "hours",  totalMinutes, "minutes worked" 


# ==================
# OUTPUT
# ==================

output = open("output.log","w")
output.write(str(firstLine) + str(extraHours) + " hours " + str(totalMinutes) + " minutes worked")


