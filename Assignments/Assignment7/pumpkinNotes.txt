11/14
Modified PurpleCircle.c to Pumpkin.c
	-changed color to orange
Added functions to HelperFunctions.c
	-InStem
	-leftEye
	-InNose
	-InMouth
Modified Makefile
	-Changed all "PurpleCircle" arguements to "Pumpkin"
