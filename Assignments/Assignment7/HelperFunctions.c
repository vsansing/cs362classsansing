/* This is a bunch of stuff to use in pixel mapping. */ 
#include <math.h> // Allows me to use the pow(x,y)  ---> x^y

int InCircle(int totalrows, int totalcols, int radius, int pixRow, int pixCol){
    int InOrOut = 0; // Integer flag for if the current pixel is in the circle or not. 
                     // 1 ==> yes, 0 ==> no.

    int Centerx = totalcols/2;
    int Centery = totalrows/2;
    int dist    = 0;            //Distance from center to pixel.

    dist = pow((pow(Centerx - pixCol,2))+(pow(Centery - pixRow,2)),0.5);

    if (dist < radius){
        InOrOut = 1;
    }

    return InOrOut;
}

int InStem(int totalRows, int totalCols, int radius, int pixRow, int pixCol) {
	int InOrOut = 0; // flag for the stem. 0 means no, 2 means yes

	//Find the top of the stem
	int dist = 0;   //distance from center to pixel
	int centerX = totalCols/2;
	int centerY = totalRows/2;

	double stemSize = radius * 0.25; // set the stem to being 1/4 of the radius
	dist = (centerX + stemSize) * (centerY + stemSize); //need to have it so it it on top of pumpkin
	if (dist < 0) {
		InOrOut = 2;
	}
	return InOrOut;
}

// Function to find the left eye of the pumpkin
int leftEye(int totalRows, int totalCols, int radius, int pixRow, int pixCol) {
	int InOrOut = 0;
	int dist = 0;

	double eyeSize = radius * .125; // the eye is 1/8 of the radius
	//dist = 0; 

	if (dist < 0) {
		InOrOut = 2;
	}
	return InOrOut;

}

//Function to find the nose of the pumpkin
int InNose(int totalRows, int totalCols, int radius, int pixRows, int pixCol) {
	int InOrOut = 0;
	int dist = 0;

	if (dist < 0) {
		InOrOut = 2;
	}
	return InOrOut;

}

int InMouth(int totalRows, int totalCols, int radius, int pixRows, int pixCol) {
	int InOrOut = 0;
	int dist = 0;
	int centerX = totalCols/2;
	int centerY = totalRows/2;
	double mouthSize = radius * 0.70;

	dist = (centerX + mouthSize) * (centerY + mouthSize);

	if (dist < 0) {
		InOrOut = 2;
	}
	return InOrOut;
}

