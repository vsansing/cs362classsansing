#!/bin/bash

# =====================
# Look for .tex files
# =====================

find . -name "*.tex" -print


# =====================
# Run the pdflatex command 
# =====================

for i in *.tex;do pdflatex $i;done


# =====================
# Remove .aux .log and .bib files
# =====================

rm *.aux
rm *.bib
rm *.log
rm *.out

# =======================
# Create a LatexCompileReport.txt file
# Complete with word count and name of
# Each file
# =======================

#insert fun code here
