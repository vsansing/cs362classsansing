#!/bin/bash

HEIGHT=15
WIDTH=40
CHOICE_HEIGHT=4
BACKTITLE="Your Options Back Title"
TITLE="Daddy"
MENU="You Must Chooooooose!"

OPTIONS=(1 "Option 1"
	 2 "Option 2"
	 3 "Option 3")

CHOICE=$(dialog --clear \
		--backtitle "$BACKTITLE" \
		--title "$TITLE" \
		--menu "$MENU" \
		$HEIGHT $WIDTH $CHOICE_HEIGHT \
		"${OPTIONS}[@]}" \
		2>&1 >/dev/tty)

clear
case $CHOICE in
	1)
		echo "You chose Option 1" ;;
	2) 
		echo "Number 2 selected" ;;
	3)
		echo "Lord Vader has blessed you with his presence at your place" ;;
esac


