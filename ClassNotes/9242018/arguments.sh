#!/bin/bash

#We will read some paramenters for the script from the command line.


echo "To used: ./argument.sh use Thing1 Thing2 Thing3"

POSPAR1="$1"
POSPAR2="$2"
POSPAR3="$3"

echo "$1 is the first postition parameter. \$1."
echo "$2 is the second postition parameter. \$2."
echo "$3 is the third postition parameter. \$3."
echo
echo "The total parameters is $#"
