#include <stdio.h>
#include <stdlib.h>
/*
   This is a c program to create a red and blue plain pixel map
   Usage:
	redBluePPM OutFileName numRows numCols

	Here our program takes in an OutFileName and two arguments the rows and 
		columns for the size of the image to produce


*/


int main(int argc, char *argv[]){
//argc is the number of things passed into the function
//*argv is the number of things passed into the array

	int numRows, numCols, imageSize;
	unsigned char *outImage; //pixel pointer
	unsigned char *ptr;	// pointer to output
//	unsigned char *outputFP;
	FILE *outputFP;
	int row, col;

	printf("====================\n");
	printf("Pixel Map           \n");
	printf("====================\n\n");

	if(argc!=4){
		printf("Usage: ./redBluePPM OutFileName numRow numCol \n");
		exit(1);
}
	if ((numRows = atoi(argv[2])) <= 0){
		printf("Error: numRows needs to be postive");
	}
	if ((numCols = atoi(argv[3])) <= 0) {
		printf("Error: numCols needs to be positive");
	}
	// ====================
	// Set up space for the ppm image
	// ====================
	imageSize = numRows * numCols * 3;
	outImage  = (unsigned char *) malloc(imageSize); // get enough space for image

	// Open a file to put the output image into 
	if (outputFP = fopen(argv[1], "w") == NULL) {
		perror("output not open error");
		printf("Error: cannot open output file\n");
		exit(1);
	}
	// Create image
	ptr = outImage;
	for(row = 0; row < numRows; row++) {
		for(col = 0; col < numCols; col++) {
		// walk on home boy (over image)
			if (col < numCols/2) {
			// Red pixel
			  *ptr     = 255;
			  *(ptr+1) = 0;
			  *(ptr+2) = 0;
			}
			else {
			// Blue pixel
			  *ptr     = 0;
			  *(ptr+1) = 0;
			  *(ptr+2) = 255;
			}
			ptr += 3;
		}

	}
	// Pull all info into a file with a need file
	fprintf(outputFP, "P6 %d %d 255\n", numCols, numRows);
	fwrite(outImage, 1, imageSize, outputFP);


	//Done
	fclose(outputFP);
	return 0;
}
