from letterFunctions import *
"""This is a script to do a mail merge to make custom letters to mythical beings.
	To Run:
	[user@machine directory]$ python MainLetterScript.py nameAddress.txt
"""


# ======================================
# READ IN COMMAND LINE ARGUMENTS
# ======================================
if len(sys.argv) != 2:
	print "To Run: python mainLetterScript.py nameAddress.txt"
else:
	print "Using address from the file", str(sys.argv[1])
	dataFile = str(sys.argv[1])

# =====================================
# OPEN FILES
# =====================================
addData = open(dataFile,"r")


# =====================================
# READ DATA FROM FILE LINE BY LINE
# =====================================

print "Reading from file " + dataFile
addressList =[]
for line in addData.readlines():
	First, Last, AD1, AD2 = map(str,line.split(','))
	CurrentAddress = Address(First, Last, AD1, AD2)
	addressList.append(CurrentAddress)
	CurrentAddress.dumpAddress()
