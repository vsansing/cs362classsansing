import sys
import os
import math
from string import whitespace
""" 
This is my library of functions that can be called in the letter writing main
"""

# ================================
# DEFINE A CLASS
# ================================
class Address:
	def __init__(self,First,Last,AD1,AD2):
		self.First = First
		self.Last = Last
		self.AD1 = AD1
		self.AD2 = AD2

	def dumpAddress(self):
		 print "Address Info:"
		 print self.First, " ", self.Last
		 print self.AD1, " \n", self.AD2

class DocumentText:
	def __init__(self,address):
		self.address = address

		self.header = ("\\documentclass[12]{article} \n"
			  	+ "\\usepackage{geometry} \n"
				+ "\\geometry{margin={1in,1in},vmargin=2in,1in}}\n"
				+ "\\begin{document} \n"
				+ "\\thispagestyle{empty} \n\n" )

		self.myAddress = "My Address \n\n\\vskip.5in \n\n"
		self.toAddress = (self.address.First + " " + self.address.Last 
				+ "\n\n" 
				+ self.address.AD1 + "\n\n"
 				+ self.address.AD2 + "\n\n")
 		self.date = "\\vskip.5in \n \\today \n\n \vskip.5in \n\n"

		self.greeting = "Dear" + self.address.First + " " + self.address.Last + ", \n\n \vskip.5in"

		self.body = ("According to all known rules of aviation, a bee " 
				+ "should not be able to fly. It's fat little body "
				+ "can not possibly get itself off the ground."
				+ "\n\n \vskip.5in \n\n \hskip3.5in Your friend \n\n"
				+ "\hskip3.5in Megatron \n\n")

		self.footer = "\\end{document}\n"

	def WriteLetter(self):
		LeterOutName = str(self.address.Last.lstrip(' ')) + ".tex"
		LetterFile = open(LetterOutName,"w")
		LetterFile.write(self.header)
		LetterFile.write(self.myAddress)
		LetterFile.write(self.toAddress)
		LetterFile.write(self.date)
		LetterFile.write(self.greeting)
		LetterFile.write(self.body)
		LetterFile.write(self.footer)
		LetterFile.close()






